/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var _ = require("underscore");
var Qfs = require("q-io/fs");
//var Q = require("q");

var read_json = require("./scripts/read-json");
//var promisify = require("./scripts/promisify");
var exec_promise = require('./scripts/exec-promise');

//var emailer = require('./scripts/emailer');
//var send_scheduled_code_review_emails = require('./scripts/send-scheduled-code-review-emails');
//var send_scheduled_testing_summary_emails = require('./scripts/send-scheduled-testing-summary-emails');
//var fetch_rpg_code_base = require('./scripts/fetch-rpg-code-base');
//var get_rpg_code_heat = require('./scripts/get-rpg-code-heat');
//var create_rpg_detailed_circles = require('./scripts/create-rpg-detailed-circles');
//var fetch_rpg_changes_for_incidents = require('./scripts/fetch-rpg-changes-for-incidents');
var get_cucumber_from_junit = require('./scripts/get-cucumber-from-junit');

(function doMainStuff() {
    var today = new Date();

    //  TEST CODE HERE ------------
    console.log('hello');
    //    emailer({
    //            to: 'alan.gordon@jhc.financial'
    //        })
    //    send_scheduled_code_review_emails()
    //        .then(function (results) {
    //            console.log('results=' + results);
    //            return true;
    //        })
    //        .fail(console.log)
    //        .done();


    //    exec_promise('svn log http://svn.jhc.co.uk/svn/jhc/thenon/F63/delphi/ -r {2017-03-01}:{2017-03-31}')
    //        .then(function (output) {
    //            //console.log('\n\n------\nOUTPUT=\n------\n' + output + '\n------');
    //            return output;
    //        })
    //        .then(function (output) {
    //            console.log('Hello Alan');
    //
    //            //var rePattern = new RegExp(/^r\d* \| (.*) \| \d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d .*line.*\n.*\n(.*)\n----------$/);
    //            //var rePattern = new RegExp(/r\d* \| (.*) \| \d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d .*line.*\n/);
    //            //var arrMatches = output.match(rePattern);
    //            //console.log('matches:' + arrMatches[1]);
    //
    //            var pattern = new RegExp(/r\d+ \| (\w+) \| \d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.* \| .+ line.*(\r\r|\n\n|\r\n\r\n).*(\d\d\d\d\d\d|\w\w\w-\d\d\d|1\d\d\d\d\d\d).*/g);
    //            //var test = 'pattern:answer1\npattern:answer2\n';
    //            //var pattern = new RegExp(/pattern:(.*)\n/g);
    //            var match = pattern.exec(output);
    //            while (match) {
    //                console.log('match=' + match[1] + ':' + match[3]);
    //                match = pattern.exec(output);
    //            }
    //            console.log('test1');
    //
    //            return true;
    //        })
    //        .fail(console.log)
    //        .done();


    //.then(fetch_rpg_changes_for_incidents)
    //.then(fetch_all_code_changes)
    //    var results = fetch_rpg_code_base()
    //        //var results = read_json('json/incident-changes-results.json')
    //        //    .then(fetch_rpg_changes_for_incidents)
    //        //    .then(function (results) {
    //        //        return Qfs.write("json/incident-changes-results.json", JSON.stringify(results, null, '\t'))
    //        //            .then(function (result) {
    //        //                return results;
    //        //            });
    //        //    })
    //        //    .then(fetch_rpg_code_base)
    //        .then(get_rpg_code_heat)
    //        .then(create_rpg_detailed_circles)
    //        .then(function (results) {
    //            console.log("we're back");
    //        })
    //        .fail(console.log)
    //        .done();


    //    var results = send_scheduled_testing_summary_emails()
    //        .then(function (results) {
    //            console.log("we're back");
    //        })
    //        .fail(console.log)
    //        .done();

    var results = get_cucumber_from_junit('./cucumber.xml')
        .then(function (results) {
            console.log("we're back");
            console.log("results = " + JSON.stringify(results, null, '  '));
        })
        .fail(console.log)
        .done();


    console.log('hello again');

    //    var startDate = new Date(2016, 11 - 1, 01);
    //    var dateRange = [];
    //    while (startDate <= today) {
    //        dateRange.push(startDate.getFullYear() * 10000 + (startDate.getMonth() + 1) * 100 + startDate.getDate());
    //        startDate.setDate(startDate.getDate() + 1);
    //        //console.log('start date now = ' + startDate);
    //    }
    //
    //    _.each(dateRange, function (aDate) {
    //        console.log('hello:' + aDate);
    //    });

})();