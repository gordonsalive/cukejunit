/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 7/6/2017
// Module to convert cucumber.json files into junit.xml
// Assumptions: 'testsuite' = 'feature', 'testcase'='scenario'
//              all steps passed = passed, any step failed = failed, anything else = test skipped
// Changes: 31/8/2017 - Exclude missing tests and empty features from the junit.xml produced.
//**********
// Notes:
// ------- Junit has this basic structure
//<?xml version="1.0" encoding="UTF-8"?>
//<testsuites>
//   <testsuite name="JUnitXmlReporter" errors="0" tests="0" failures="0" time="0" timestamp="2013-05-24T10:23:58" />
//   <testsuite name="JUnitXmlReporter.constructor" errors="0" skipped="1" tests="3" failures="1" time="0.006" timestamp="2013-05-24T10:23:58">
//      <properties>
//         <property name="java.vendor" value="Sun Microsystems Inc." />
//         <property name="compiler.debug" value="on" />
//         <property name="project.jdk.classpath" value="jdk.classpath.1.6" />
//      </properties>
//      <testcase classname="JUnitXmlReporter.constructor" name="should default path to an empty string" time="0.006">
//         <failure message="test failure">Assertion failed</failure>
//      </testcase>
//      <testcase classname="JUnitXmlReporter.constructor" name="should default consolidate to true" time="0">
//         <skipped />
//      </testcase>
//      <testcase classname="JUnitXmlReporter.constructor" name="should default useDotNotation to true" time="0" />
//   </testsuite>
//</testsuites>
// ------- cucumber has this basic structure:
//[
//  {
//    "uri": "features/one_passing_one_failing.feature",
//    "keyword": "Feature",
//    "id": "one-passing-scenario,-one-failing-scenario",
//    "name": "One passing scenario, one failing scenario",
//    "line": 2,
//    "description": "",
//    "tags": [
//      {
//        "name": "@a",
//        "line": 1
//      }
//    ],
//    "elements": [
//      {
//        "keyword": "Scenario",
//        "id": "one-passing-scenario,-one-failing-scenario;passing",
//        "name": "Passing",
//        "line": 5,
//        "description": "",
//        "tags": [
//          {
//            "name": "@b",
//            "line": 4
//          }
//        ],
//        "type": "scenario",
//        "steps": [
//          {
//            "keyword": "Given ",
//            "name": "this step passes",
//            "line": 6,
//            "match": {
//              "location": "features/step_definitions/steps.rb:1"
//            },
//            "result": {
//              "status": "passed",
//              "duration": 1
//            }
//          }
//        ]
//      },
//      {
//        "keyword": "Scenario",
//        "id": "one-passing-scenario,-one-failing-scenario;failing",
//        "name": "Failing",
//        "line": 9,
//        "description": "",
//        "tags": [
//          {
//            "name": "@c",
//            "line": 8
//          }
//        ],
//        "type": "scenario",
//        "steps": [
//          {
//            "keyword": "Given ",
//            "name": "this step fails",
//            "line": 10,
//            "match": {
//              "location": "features/step_definitions/steps.rb:4"
//            },
//            "result": {
//              "status": "failed",
//              "error_message": " (RuntimeError)\n./features/step_definitions/steps.rb:4:in /^this step fails$/'\nfeatures/one_passing_one_failing.feature:10:in Given this step fails'",
//              "duration": 1
//            }
//          }
//        ]
//      }
//    ]
//  }
//]


//var Q = require("q");
//var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

var read_json = require('./read-json');
//var promisify = require('./scripts/promisify');

module.exports = function (inputFile) {
    if (DEBUG) console.log('get-junit-from-cucumber(' + inputFile + ')');
    return read_json(inputFile)
        .then(function (cucumberJson) {
            //we're going to spin through features calculating testsuite items,
            //and inside those we're going to spint thorugh scenarios calculating testcase items
            //spin through steps to figure out if testcase passed, failed or skipped
            //write out whole testsuite at the end of each one (as we need to know number of test cases, passes, fails and skipped)
            var testsPassed = 0;
            var testsFailed = 0;
            var testsSkipped = 0;
            var results = _.map(_.where(cucumberJson, {
                keyword: "Feature"
            }), function (feature) {
                return {
                    name: replaceSpecialChars(feature.name),
                    duration: feature.duration,
                    //scenarios: _.map(_.where(feature.elements, {
                    //    type: "scenario"
                    //}), function (scenario) {
                    scenarios: _.map(_.filter(feature.elements, function (element) {
                        return ((element.type === 'scenario') || (element.keyword.includes('Scenario')));
                    }), function (scenario) {
                        var isMissingScenario = false;
                        if (scenario.tags) {
                            isMissingScenario = _.pluck(scenario.tags, 'name').includes('@missing');
                        }
                        var result = {
                            name: replaceSpecialChars(scenario.name),
                            duration: scenario.duration,
                            status: (!(scenario.steps) || (scenario.steps.length === 0)) ? 
                                'skipped' : _.reduce(scenario.steps, function (memo, step) {
                                    if (step.result.status === '') {
                                        return 'skipped';
                                    } else if ((step.result.status === 'passed') &&
                                        (memo === 'passed') &&
                                        (!isMissingScenario)) {
                                        return 'passed';
                                    } else if (((step.result.status === 'failed') || (memo === 'failed')) &&
                                        (!isMissingScenario)) {
                                        return 'failed';
                                    } else {
                                        return 'skipped';
                                    }
                                }, 'passed')
                        };
                        if (result.status === 'passed') testsPassed++;
                        if (result.status === 'failed') testsFailed++;
                        if (result.status === 'skipped') testsSkipped++;
                        return result;
                    })
                };
            });
            console.log(inputFile + ': ' + results.length + ' features, ' + testsPassed + ' tests passed, ' + testsFailed + ' tests failed and ' + testsSkipped + ' tests skipped.');
            return results;
        })
        .then(function (results) {
            //filter out any features that have no scenarios
            return _.filter(results, function (feature) {
                var noOfSkippedTests = _.where(feature.scenarios, {
                    status: 'skipped'
                }).length;
                return (feature.scenarios.length > noOfSkippedTests);
            });
        })
        .then(function (results) {

            //now have an array of features each with an array of scenarios: 
            // [{name: "some feature", scenarios: [{name: "some scenario", status: "passed"}]}]
            var output = '';
            output += '<?xml version="1.0" encoding="UTF-8"?>\n';
            output += '<testsuites>\n';
            output += _.map(results, function (feature) {
                //var passedTests = _.where(feature.scenarios, {status: 'passed'});
                //var failedTests = _.where(feature.scenarios, {status: 'failed'});
                //var skippedTests = _.where(feature.scenarios, {status: 'skipped'});
                var noOfTests = feature.scenarios.length;
                var noOfFailedTests = _.where(feature.scenarios, {
                    status: 'failed'
                }).length;
                var noOfSkippedTests = _.where(feature.scenarios, {
                    status: 'skipped'
                }).length;
                //console.log(feature.name + ': ' + noOfTests + ' tests, of which ' + noOfFailedTests + ' failed and ' + noOfSkippedTests + ' skipped.');
                //console.log('feature = ' + JSON.stringify(feature, null, ' '));

                //<testsuite name="JUnitXmlReporter.constructor" errors="0" skipped="1" tests="3" failures="1" time="0.006" timestamp="2013-05-24T10:23:58">
                //  <testcase classname="JUnitXmlReporter.constructor" name="should default path to an empty string" time="0.006">
                //    <failure message="test failure">Assertion failed</failure>
                //  </testcase>
                //  <testcase classname="JUnitXmlReporter.constructor" name="should default consolidate to true" time="0">
                //    <skipped />
                //  </testcase>
                //  <testcase classname="JUnitXmlReporter.constructor" name="should default useDotNotation to true" time="0" />
                //</testsuite>
                return '<testsuite name="' + feature.name + '" errors="0" skipped="' + noOfSkippedTests +
                    '" tests="' + noOfTests +
                    '" failures="' + noOfFailedTests +
                    //'" time="' + (feature.duration ? feature.duration : '0.001') +
                    '" time="0.01' + 
                    '" timestamp="2013-05-24T10:23:58">\n' +
                    _.map(feature.scenarios, function (scenario) {
                        //console.log('scenario = ' + JSON.stringify(scenario, null, ' '));
                        if (scenario.status === 'failed') {
                            return '  <testcase classname="' + feature.name + 
                                '" name="' + scenario.name + 
                                //'" time="' + (scenario.duration ? scenario.duration : '0.006') + '">\n' +
                                '" time="0.006">\n' +
                                '    <failure message="test failure">test failed</failure>\n' +
                                '  </testcase>';
                        } else if (scenario.status === 'skipped') {
                            // Silk Central doesn't understand skipped tests and shows them as passing
                            // ...so, leave these out
                            //return '  <testcase classname="' + feature.name + '" name="' + scenario.name + '" time="' + //(scenario.duration ? scenario.duration : '0.006') + '">\n' +
                            //    '    <skipped />\n' +
                            //    '  </testcase>';
                            return '';
                        } else {
                            return '  <testcase classname="' + feature.name + 
                                '" name="' + scenario.name + 
                                //'" time="' + (scenario.duration ? scenario.duration : '0.006') + '" />';
                                '" time="0.006" />';
                        }
                    }).join('\n') +
                    '\n</testsuite>';
            }).join('\n');

            output += '\n</testsuites>';
            return output;
        });
};

function replaceSpecialChars(name) {
    if (DEBUG) console.log('replaceSpecialChars(' + name + ')');
    return (name === '') ? '_blank_' : name
        .replace(/&/g, 'and')
        .replace(/[`¬¦!"£$%^&*()+={[\]}:;@'~#<,>?\/]/g, '_');
}