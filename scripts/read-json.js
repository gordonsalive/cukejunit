/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Qfs = require("q-io/fs");
var Q = require("q");

var DEBUG = 0; //0=off

// AAG 31/3/'16
// method to asynchronously read the json out of a file and 
// return a promise of the contents parse as an json object
// TODO: need some tests just for this module
module.exports = function (filename) {
    if (DEBUG) console.log('read-json from file:' + filename);
    //when file is read, then, parse the json and return as a promise so it can be chained.
    return Qfs.read(filename).then(function (data) {
        return Q.Promise(function (resolve, reject, notify) {
            try {
                resolve(JSON.parse(data));
            } catch (err) {
                reject(err);
            }
        });
    });
};